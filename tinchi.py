﻿import requests, json
from bs4 import BeautifulSoup
from hashlib import md5
from prettytable import PrettyTable

password = 'Wtf123#'
username = '1851160076'

s = requests.session()

def login(username, password):
    hashed = md5(password).hexdigest()
    r = s.get('http://dkh.tlu.edu.vn/CMCSoft.IU.Web.Info/Login.aspx?url=http://dkh.tlu.edu.vn/CMCSoft.IU.Web.Info/Home.aspx')

    soup = BeautifulSoup(r.text, 'html.parser')
    post_data = get_default_post_data(soup, 'Form1')

    post_data['txtUserName'] = username
    post_data['txtPassword'] = hashed

    r1 = s.post('http://dkh.tlu.edu.vn/CMCSoft.IU.Web.Info/Login.aspx?url=http%3A%2F%2Fdkh.tlu.edu.vn%2FCMCSoft.IU.Web.Info%2FHome.aspx', data=post_data, allow_redirects=False)
    if (r1.status_code == 302):
        location = r1.headers['Location']
        if (location == 'http://dkh.tlu.edu.vn/CMCSoft.IU.Web.Info/Home.aspx'):
            return True
    return False

def get_default_post_data(soup, element_id):
    inputs = soup.find(id=element_id).find_all('input')
    post_data = {}
    
    for i in inputs:
        if 'value' in i.attrs and 'name' in i.attrs:
            post_data[i.attrs['name']] = i.attrs['value']

    return post_data
    
def get_calendar(key, weekday=0):
    # weekday: 2->8 = Monday -> Sunday. 0 = All
    r = s.get('http://dkh.tlu.edu.vn/CMCSoft.IU.Web.info/StudyRegister/StudyRegister.aspx')

    soup = BeautifulSoup(r.text, 'html.parser')
    post_data = get_default_post_data(soup, 'Form1')
    post_data['drpCourse'] = key
    post_data['drpWeekDay'] = weekday
    for key in post_data.keys():
        if 'btn' in key and key != 'btnViewCourseClass':
            del post_data[key]

    r1 = s.post('http://dkh.tlu.edu.vn/CMCSoft.IU.Web.info/StudyRegister/StudyRegister.aspx', data=post_data)
    soup1 = BeautifulSoup(r1.text, 'html.parser')
    rows = soup1.find(id='Table1').find_all(class_=['cssListItem', 'cssListAlternativeItem'])
    full = []
    
    for row in rows:
        cooked_row = {}
        cooked_keys = ['stt', 'chon', 'lop_hoc_phan', 'hoc_phan', 'thoi_gian', 'dia_diem', 'giang_vien', 'si_so', 'da_dang_ky']
        tds = row.find_all('td')
        i = 0
        for td in tds:
            radio = td.find(id='rdiSelect')
            if radio:
                cooked_row[cooked_keys[i]] = {'name': radio['name'], 'value': radio['value']}
            else:
                cooked_row[cooked_keys[i]] = td.text.encode('utf-8').strip().replace('\xc2\xa0\xc2\xa0', '\n')
            i += 1
        full.append(cooked_row)
    return full, soup1

def get_registered():
    r = s.get('http://dkh.tlu.edu.vn/CMCSoft.IU.Web.info/StudyRegister/StudyRegister.aspx')

    soup = BeautifulSoup(r.text, 'html.parser')
    rows = soup.find(id='Table4').find_all(class_='cssRangeItem3')
    full = []
    
    for row in rows:
        cooked_row = {}
        cooked_keys = ['stt', 'chon', 'lop_hoc_phan', 'hoc_phan', 'thoi_gian', 'dia_diem', 'giang_vien', 'si_so', 'da_dang_ky', 'so_tc', 'hoc_phi', 'ghi_chu']
        tds = row.find_all('td')
        i = 0
        for td in tds:
            key = td.find(id='hidRgsCourseId')
            if key:
                cooked_row[cooked_keys[i]] = key['value']
            else:
                cooked_row[cooked_keys[i]] = td.text.encode('utf-8').strip().replace('\xc2\xa0\xc2\xa0', '\n')
            i += 1
        full.append(cooked_row)
    return full

def get_subjects():
    r2 = s.get('http://dkh.tlu.edu.vn/CMCSoft.IU.Web.info/StudyRegister/StudyRegister.aspx')
    soup2 = BeautifulSoup(r2.text, 'html.parser')
    options = soup2.find(id='drpCourse').find_all('option')[1:]
    full = []
    for option in options:
        if 'value' in option.attrs and option.attrs['value'].strip() != "":
            full.append({'key': option.attrs['value'], 'name': option.text.encode('utf-8')})

    return full

def register(key, radio, index, calendar_soup):
    index = int(index)
    index += 2
    if (index<10):
        index = '0'+ str(index)
    else:
        index = str(index)
    r = s.get('http://dkh.tlu.edu.vn/CMCSoft.IU.Web.info/StudyRegister/StudyRegister.aspx')

    soup = BeautifulSoup(r.text, 'html.parser')
    post_data = get_default_post_data(calendar_soup, 'Form1')
    post_data.update(get_default_post_data(calendar_soup, 'Table2'))
    post_data['hidCourseId'] = key
    post_data['drpCourse'] = key
    post_data[radio['name']] = radio['value']
    post_data['gridRegistration$ctl'+index+'$chkSelect'] = 'on'
    post_data['hidClientNumberOfCourse'] = 1

    for key in post_data.keys():
        if 'btn' in key and key != 'btnUpdate':
            del post_data[key]

    r2 = s.post('http://dkh.tlu.edu.vn/CMCSoft.IU.Web.info/StudyRegister/StudyRegister.aspx', data=post_data)

def cancel(key, index):
    index = int(index)
    index += 2
    if (index<10):
        index = '0'+ str(index)
    else:
        index = str(index)
    r = s.get('http://dkh.tlu.edu.vn/CMCSoft.IU.Web.info/StudyRegister/StudyRegister.aspx')

    soup = BeautifulSoup(r.text, 'html.parser')
    post_data = get_default_post_data(soup, 'Form1')
    post_data.update(get_default_post_data(soup, 'Table2'))
    post_data['gridRegistered$ctl'+index+'$chkDelete'] = 'on'
    post_data['hidClientNumberOfCourse'] = 1

    for key in post_data.keys():
        if 'btn' in key and key != 'btnCancel':
            del post_data[key]

    r2 = s.post('http://dkh.tlu.edu.vn/CMCSoft.IU.Web.info/StudyRegister/StudyRegister.aspx', data=post_data)

def print_registered(registered):
    column_names = ['stt', 'lop_hoc_phan', 'hoc_phan', 'thoi_gian', 'dia_diem', 'si_so', 'da_dang_ky', 'so_tc', 'hoc_phi']
    t = PrettyTable(column_names)
    for column_name in column_names:
        t.align[column_name] = 'l'
        
    for line in registered:
        row = []
        for name in column_names:
            row.append(line[name])
        t.add_row(row)
    print t

def print_calendar(calendar):
    column_names = ['stt', 'lop_hoc_phan', 'hoc_phan', 'thoi_gian', 'dia_diem', 'giang_vien', 'si_so', 'da_dang_ky']
    t = PrettyTable(column_names)

    for column_name in column_names:
        t.align[column_name] = 'l'
        
    for line in calendar:
        row = []
        for name in column_names:
            row.append(line[name])
        t.add_row(row)
    print t
    
if __name__ == '__main__':
    login(username, password)
    mode = int(raw_input('''Chọn chức năng:
    1. Đăng ký học phần
    2. Xem học phần đã đăng ký
    3. Hủy đăng ký học phần
Chọn: '''))
    if (mode == 1):
        subjects = get_subjects()
        for i in range(0, len(subjects)):
            print str(i+1)+'. '+subjects[i]['name']
        subject_index = int(raw_input('Nhập số thứ tự môn cần đăng ký: '))-1
        calendar, calendar_soup = get_calendar(subjects[subject_index]['key'])
        print len(calendar)
        print_calendar(calendar)

        class_index = raw_input('Nhập số thứ tự học phần cần đăng ký: ')
        real_index = None
        for i in range(0, len(calendar)):
            if (calendar[i]['stt'] == class_index):
                real_index = i
                break
        if real_index == None:
            print 'Không tìm thấy học phần'
        else:
            _class = calendar[real_index]
            register(subjects[subject_index]['key'], _class['chon'], real_index, calendar_soup)
            print('Đăng ký thành công!')
    elif (mode == 2):
        registered = get_registered()
        print('Bạn đã đăng ký những học phần sau:')
        print_registered(registered)
    elif (mode == 3):
        registered = get_registered()
        print('Bạn đã đăng ký những học phần sau:')
        print_registered(registered)
        cancel_index = raw_input('Nhập số thứ tự học phần cần hủy: ')
        real_index = None
        for i in range(0, len(registered)):
            if (registered[i]['stt'] == cancel_index):
                real_index = i
                break
        if real_index == None:
            print 'Không tìm thấy học phần'
        else:
            cancel(registered[real_index]['chon'], real_index)
            print('Hủy thành công')
