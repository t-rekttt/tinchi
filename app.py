import os
from flask import Flask, request

app = Flask(__name__)
app.debug = True

messenger = Messenger(os.environ.get('FB_VERIFY_TOKEN'), os.environ.get('FB_PAGE_TOKEN'))

@app.route('/webhook', methods=['GET', 'POST'])
def webhook():
  if request.method == 'GET':
    if (request.args.get('hub.verify_token') == os.environ.get('FB_VERIFY_TOKEN')):
      return request.args.get('hub.challenge')
    raise ValueError('FB_VERIFY_TOKEN does not match.')
  elif request.method == 'POST':
    messenger.handle(request.get_json(force=True))
  return ''


if __name__ == "__main__":
  app.run(host='0.0.0.0')